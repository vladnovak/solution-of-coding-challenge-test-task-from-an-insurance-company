package pl.novak;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class InsuranceServiceGrouping {

    Map<String, String[]> processFile(InputStream inputStream) {
        Predicate<String> filter = line ->
                !line.toUpperCase().startsWith("SKIP") && !line.isBlank();

        Function<String, String[]> mapper = line ->
                Arrays.stream(line.split(";"))
                        .map(String::trim)
                        .map(String::toLowerCase)
                        .distinct()
                        .toArray(String[]::new);

        Collector<String[], ?, Map<String, String[]>> collector = Collectors
                .toMap(
                        elem -> elem[0],
                        elem -> Arrays.copyOfRange(elem, 1, elem.length),
                        (existingValues, mergingValues) ->
                                Stream.of(existingValues, mergingValues)
                                        .flatMap(Stream::of)
                                        .distinct()
                                        .toArray(String[]::new)
                );

        StreamProcessor processor = new StreamProcessor
                .StreamProcessorBuilder()
                .filter(filter)
                .mapper(mapper)
                .collector(collector)
                .build();

        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        return processFile(bufferedReader, processor);
    }

    Map<String, String[]> processFile(BufferedReader bufferedReader, StreamProcessor processor) {
        return bufferedReader
                .lines()
                .filter(processor.getFilter())
                .map(processor.getMapper())
                .collect(processor.getCollector());
    }
}