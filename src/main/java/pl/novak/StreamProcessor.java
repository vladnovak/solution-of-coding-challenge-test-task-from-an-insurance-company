package pl.novak;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;

class StreamProcessor {
    private final Predicate<String> filter;
    private final Function<String, String[]> mapper;
    private final Collector<String[], ?, Map<String, String[]>> collector;

    StreamProcessor() {
        this.filter = null;
        this.mapper = null;
        this.collector = null;
    }

    StreamProcessor(StreamProcessorBuilder builder) {
        this.filter = builder.filter;
        this.mapper = builder.mapper;
        this.collector = builder.collector;
    }

    public static class StreamProcessorBuilder {
        private Predicate<String> filter;
        private Function<String, String[]> mapper;
        private Collector<String[], ?, Map<String, String[]>> collector;

        StreamProcessorBuilder filter(Predicate<String> filter) {
            this.filter = filter;
            return this;
        }

        StreamProcessorBuilder mapper(Function<String, String[]> mapper) {
            this.mapper = mapper;
            return this;
        }

        StreamProcessorBuilder collector(Collector<String[], ?, Map<String, String[]>> collector) {
            this.collector = collector;
            return this;
        }

        StreamProcessor build() {
            return new StreamProcessor(this);
        }
    }

    Predicate<String> getFilter() {
        return filter;
    }

    Function<String, String[]> getMapper() {
        return mapper;
    }

    Collector<String[], ?, Map<String, String[]>> getCollector() {
        return collector;
    }
}
