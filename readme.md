# Solution of coding challenge/test task from an insurance company

### Grouping of Insurance Services

CODE TASK FOR 100 PTS

### Description
You are involved in creating an application for grouping insurance services based on logs saved in a text file. The application assumptions were as follows:

- The input stream contains lines of words separated by a semicolon.
- The first word is the name of the city where the branch is located.
- The next words are the services assigned to it.
- The application returns a map in the form: ```city -> list of services``` in any order.
- The application has been written, but unfortunately, it does not work properly.

### Task
Correct the code so that:

- Every city and service were stripped of blank signs front and back.
- Correctly grouped services even if their definitions were repeated in several lines and were written in different case letters (he reduced the names of services and cities to lowercase letters).
- Removed duplicate services within one city, even if they are written in different case letters.
- Ignored blank lines.
- Ignored lines that start with the word ```SKIP```.
- Acted according to the example.

To solve the task, all you need to do is edit the ```InsuranceServiceGrouping class```.

Sample input data content:
```
SKIPwarszawa;oc;zdrowotne

bielsko-biała;na życie ;od powodzi

łódź; od ognia;OD NIESZCZĘŚLIWYCH WYPADKÓW;ac



 ŁÓDŹ;domu;na wypadek straty pracy;Ac
```
Resulting map object:
```
"bielsko-biała" -> ["na życie", "od powodzi"]

"łódź" -> ["od ognia","od nieszczęśliwych wypadków","ac","domu","na wypadek straty pracy"]
```

Limitations:
- 0 < number of input data lines < 1000
- 0 <= number of characters in a single line of input data < 10000